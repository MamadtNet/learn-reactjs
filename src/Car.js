import React from "react";
import './test.css'
import './mysass.scss' // React Will be compile Sass to Css
class Car extends React.Component { //Can be function too
    constructor(props) {
        super(props);
        this.state = {
            brand: 'Benz',
            color: "red"
        }

    }

    render() {
        return (
            <React.Fragment>
                <h1>Render of Car.js use props : {this.props.name}</h1>
                <p>
                    <p style={{color: "red"}}>You can called component in component as you see</p>
                    car model in props : {this.props.carDetail.model}
                    <br/>car color in props : {this.props.carDetail.color}
                    <br/>We can input App props in Car state : {this.props.Appstate.name}
                </p>
            </React.Fragment>
        )
    }
}


export default Car