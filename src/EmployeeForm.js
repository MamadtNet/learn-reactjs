import React from "react"
import axios from "axios"

class EmployeeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            salary: '',
            age: ''
        }
    }
    changeHandler = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    }
    handleSubmit = (event) => {
        event.preventDefault()
        let url = 'http://dummy.restapiexample.com/api/v1/create'
        axios.post(url, this.state)
            .then(response => {
                    alert(response.status)
            })
            .catch(error => console.log(error))
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div>
                    name :<input type={'text'} name={'name'} onChange={this.changeHandler} value={this.state.name}/>
                </div>
                <div>
                    salary :<input type={'text'} name={'salary'} onChange={this.changeHandler} value={this.state.salary}/>
                </div>
                <div>
                    age :<input type={'text'} name={'age'} onChange={this.changeHandler} value={this.state.age}/>
                </div>
                <button type={'submit'}>Save</button>
            </form>
        )
    }
}

export default EmployeeForm