import React from "react";

const ForwardingInput = React.forwardRef((props, ref) => {
    return (
        <input ref={ref}/>
    )
})

export default ForwardingInput