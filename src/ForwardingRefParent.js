import React from "react";
import ForwardingInput from "./ForwardingRefInput";

class ForwardingRefParent extends React.Component {
    constructor(props) {
        super(props);
        this.inputRef = React.createRef()
    }

    clickHandler = () => {
        this.inputRef.current.focus()
    }

    render() {
        return (
            <div>
                <ForwardingInput ref={this.inputRef}/>
                <button onClick={this.clickHandler}>Focus</button>
            </div>
        )
    }

}

export default ForwardingRefParent