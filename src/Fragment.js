import React from "react";
import TdComp from "./TdComp";
import PureComponent from "./PureComponent";

//Fragment - pure Com
class Fragment extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <> {/*take as React.Fragment*/}
                <hr/>
                <h1 style={{textAlign: "center", color: "red"}}>Fragment - pure Com </h1>
                <table>
                    <tr>
                        <TdComp/>
                    </tr>
                </table>
                <PureComponent/>
            </>


        )
    }
}

export default Fragment