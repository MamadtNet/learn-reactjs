import React from "react";

class LearnFormCssSass extends React.Component {
    constructor(props) {
        super(props);
        this.state = { // you must saved Forms value in state
            myName: "Mamad",
            checked: true,
            text: "Please Enter Something",
            select: "Porsche"
        }
    }

    handleChange = (event) => {
        let name = event.target.name
        let value = event.target.type === "checkbox" ? event.target.checked : event.target.value
        this.setState({
                [name]: value //if you have a dynamic value use []
        })
        if (event.target.type === "textarea") {
            this.setState({
                text: event.target.value
                })
        }
        console.log(event.target.type)
        if (event.target.type === "select-one") {
            this.setState({
                    select: event.target.value
            })
        }
    }
    handleSubmit = (event) => {
        // e    vent.preventDefault() // go back to default
        // alert("Form Has Been Submitted")
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log(this.state.checked)
        console.log(this.state.myName)
        console.log(this.state.text)
        console.log(this.state.select)
    }

    render() {
        // you can use render conditional
        let header = ""
        if (this.state.myName !== ''){
            header = "===== Welcome" + this.state.myName + "====="
        }else {
            header = "=====Goodbye====="
        }
        //Use java Object
        const myStyle = { //use Conditional
            color : "#4B0302",
            backgroundColor : "#59d2d2",
            padding : "10px",
            fontFamily : "Arial"
        }
        return (
            <form onSubmit={this.handleSubmit} style={myStyle}>
                <h1 style={{textAlign : "center"}}>Form, Css, Sass in React.JS</h1>
                {header}
                <br/><br/>
                    <label>Name : </label>
                    <input type="text" name={"myName"} value={this.state.myName} onChange={this.handleChange}/>
                    <input type={"checkbox"} name={"chk"} checked={this.state.checked} onChange={this.handleChange}/>
                    <textarea value={this.state.text} name={"txt"} onChange={this.handleChange}/>
                    <select value={this.state.select} onChange={this.handleChange}> {/*Selected What You Want As Default */}
                        <option>Pride</option>
                        <option>Benz</option>
                        <option>Porsche</option>
                        <option>BMW</option>
                    </select>
                    <input type={"text"} value={this.state.myName} onChange={this.handleChange}/>
                    <input type={"submit"} value={"submit"}/>
            </form>
        )
    }
}




export default LearnFormCssSass