import React from "react";
import ReactDOM from "react-dom"

const PortalDemo = () => { // Can be in class !!
    return ReactDOM.createPortal(
        <h2>h2 in portalDemo Div (inspect it)</h2>
        , document.getElementById('portal-demo')
    )
}
export default PortalDemo

