import React from "react";
import FocusInput from "./FocusInput";
import ForwardingRefParent from "./ForwardingRefParent";

class RefDemo extends React.Component {
    constructor(props) {
        super(props);
        this.inputRef = React.createRef()
        this.callBackRef = null
        this.setCallBackRef = (element) => {
            this.callBackRef = element
        }
    }

    componentDidMount() {
        this.inputRef.current.focus()
        console.log(this.inputRef)
        if (this.callBackRef){ // not null
            this.callBackRef.focus()
        }
    }
    clickHandler = () => {
        alert(this.inputRef.current.value) // you can get Elements with ref!
    }

    render() {
        return (
            <div>
                <hr/>
                <h1 style={{textAlign : "center", color : "red"}}>Refs</h1>
                <input type={"text"} ref={this.inputRef}/>
                <button onClick={this.clickHandler}>Send!</button>{/*First Way for focus*/}
                <br/>
                <input type={"text"} ref={this.setCallBackRef}/>{/*Second Way for focus*/}
                <br/>
                <h3 style={{textAlign : "center"}}>Using ref Component</h3>
                <FocusInput/>
                <br/>
                <h3 style={{textAlign : "center"}}>Forwarding Ref</h3>
                <ForwardingRefParent/>
            </div>
        )
    }


}

export default RefDemo