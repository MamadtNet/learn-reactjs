import React from "react";

const TdComp = () => {
    return (
        <React.Fragment> {/*You must Use React.Fragment --- and you can add Key in prop of Fragment*/}
            <td>
                Name :
            </td>
            <td>
                Ali
            </td>
        </React.Fragment>
    )
}

export default TdComp