import React from "react";
import Test2 from './Test2'
function Test1(props) {
    if (props.myName === '') //You can Create an Error !
        throw new Error('What ?!')
    return (
        <div>
            <h1>Home Page</h1>
            <h3>{props.myName}</h3>
            <Test2/>
        </div>
    )
}

export default Test1