import React from "react";

class UpdateExample extends React.Component {
    constructor(props) {
        super(props);
        this.state = {favoritecolor: "red"};
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({favoritecolor: "yellow"})
        }, 1000)
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        document.getElementById("div1").innerHTML =
            "Before the update, the favorite was " + prevState.favoritecolor;
        return null
    }

    componentDidUpdate() {
        document.getElementById("div2").innerHTML =
            "The updated favorite is " + this.state.favoritecolor;
    }

    render() {
        return (
            <div>
                <h1>My Favorite Color is {this.state.favoritecolor}</h1>
                <div id="div1"></div>
                <div id="div2"></div>
            </div>
        );
    }
}
// UpdateExample : tasks do is :
// 1) render -> 2) componentDidMount -> 3) wait 1second('setTimeout') -> 4) setState
// -> 5) getSnapshotBeforeUpdate -> 6) componentDidUpdate -> 7) render

export default UpdateExample