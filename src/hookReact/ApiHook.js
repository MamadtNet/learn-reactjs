import React, {useEffect, useState} from "react";
import axios from "axios";

function ApiHook() {
    const [employees, setEmployees] = useState([])
    //--------------------------------------------------
    const [id, setId] = useState(1)
    const [employee, setEmployee] = useState([])
    const [idFromButton, setIdFromButton] = useState(1)
    useEffect(() => {
        axios.get(`http://dummy.restapiexample.com/api/v1/employees`)
            .then(response => {
                console.log(response.data)
                setEmployees(response.data.data)
            })
            .catch(error => console.log(error))
    }, [])
    //---------------------------------------------------
    useEffect(() => {
        axios.get(`http://dummy.restapiexample.com/api/v1/employee/${id}`)
            .then(response => {
                console.log(response.data)
                setEmployee(response.data.data)
            })
            .catch(error => console.log(error))
    }, [idFromButton])
    const handleClick = () => {
        if (employee.message !== "Error Occured! Page Not found, contact rstapi2example@gmail.com")
            setIdFromButton(id)
    }
    return (
        <div>
            <br/>
            <h3>Employee in Api Hook this time :</h3>
            {
                employees.map((employee) => {
                    return (
                        <p>{employee.employee_name}</p>
                    )
                })
            }
            <hr/>
            search by id : <input value={id} onChange={(e) => setId(e.target.value)}/>
            <button onClick={handleClick}>Update</button>
            <p>{employee.employee_name}</p>

        </div>
    )
}

export default ApiHook
