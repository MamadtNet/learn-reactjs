import React, {useEffect, useReducer} from "react";
import axios from 'axios'
const initalValue = {
    loading: true,
    error: '',
    employee: {},

}

const reducer = (state, action) => {
    switch (action.type) {
        case 'success' :
            return {
                loading: false,
                error: '',
                employee: action.data
            }
        case 'error':
            return {
                loading: false,
                error: 'There is an Error',
                employee: {}
            }
        default :
            return state
    }
}

function ApiReducer() {
    const [state, dispath] = useReducer(reducer, initalValue)
    useEffect(() => {
        axios.get('http://dummy.restapiexample.com/api/v1/employee/1')
            .then(response => dispath({type : 'success', data : response.data.data}))
            .catch(error => dispath({type : 'error'}))

    }, [])
    return (
        <div>
            <h2>ApiReducer</h2>
            {state.loading ? 'loading' : state.employee.employee_name}
            {state.error ? state.error : null}
        </div>
    )

}

export default ApiReducer