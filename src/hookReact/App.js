import React, {useReducer} from "react";
import HookCounter from "./HookCounter";
import MouseContainer from "./MouseContainer";
import HookTimer from "./HookTimer";
import ApiHook from "./ApiHook";
import ComponentA from "./ComponentA";
import ReducerHook from "./ReducerHook";
import ApiReducer from "./ApiReducer";
import ParentComponent from "./component/ParentComponent"
import Counter from "./component/Counter";
import "./hook.css"
import Focus from "./Focus";
import TimerHook from "./TimerHook";
import NewCounterHook from "./NewCounterHook";
import CounterHook2 from "./CounterHook2";

//-----------------------Hooks------------------------------
export const UserContext = React.createContext()
export const SalaryContext = React.createContext()

//useReducer and useContext
export const CounterContext = React.createContext()

const initialValue = 0
const reducer = (state, action) => {
    switch (action) {
        case 'increment' :
            return state + 1
        case 'decrement' :
            return state - 1
        default :
            return state
    }
}

function App() {
    const [counter, dispatch] = useReducer(reducer, initialValue)
    return (
        <div className={"App"}>
            <hr/>
            <h1 style={{textAlign: "center", color: "Blue"}}>Introducing Hooks</h1>
            <HookCounter/>
            <MouseContainer/>
            <HookTimer/>
            <ApiHook/>

            <UserContext.Provider value={"Mamad"}>
                <SalaryContext.Provider value={"5000$"}>
                    <ComponentA/>
                </SalaryContext.Provider>
            </UserContext.Provider>
            {/*Reducer*/}
            {/*    const array1 = [1, 2, 3, 4]*/}
            {/*    const x = array1.reduce(((num1, num2) => num1 + num2), defaultValue)*/}
            {/*    */}
            {/**/}
            <ReducerHook/>
            <CounterContext.Provider value={{countState: counter, countDispatch: dispatch}}>
                <h2>useContext and useReducer</h2>
                Counter : {counter}
                <ComponentA/>
            </CounterContext.Provider>
            <ApiReducer/>
            <ParentComponent/>
            <Counter/>
            <Focus/>
            <TimerHook/>
            <NewCounterHook/>
            <CounterHook2/>
            <h1>The End</h1>
        </div>
    )
}

export default App