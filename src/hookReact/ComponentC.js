//Hook Context
import React, {useContext} from "react";
import {SalaryContext, UserContext} from "./App";
import {CounterContext} from "./App";

function ComponentC() {
    const cCountext = useContext(CounterContext)
    return (
        <>
            {/*<UserContext.Consumer>*/}
            {/*    {user => {*/}
            {/*        return (*/}
            {/*            <SalaryContext.Consumer>*/}
            {/*                {*/}
            {/*                    salary => {*/}
            {/*                        return (*/}
            {/*                            <p>Name : {user}, Salary : {salary}</p>*/}
            {/*                        )*/}
            {/*                    }*/}
            {/*                }*/}
            {/*            </SalaryContext.Consumer>*/}
            {/*        )*/}
            {/*    }}*/}

            {/*</UserContext.Consumer>*/}
            <div>
                <button onClick={() => cCountext.countDispatch('increment')}>Increment</button>
                <button onClick={() => cCountext.countDispatch('decrement')}>Decrement</button>
            </div>
        </>
    )

}

export default ComponentC