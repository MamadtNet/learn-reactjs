import React, {useState, useEffect} from "react";

function HookCounter() {
    const [count, setCounter] = useState(0) // return [value of state, function for updating]
    // {const [one, two] = [1, 2]} === {one = 1, two = 2}

    const updateFive = () => {
        for (let i = 0; i < 5; i++) {
            setCounter(prevState => prevState + 1) //this is the right way
        }
    }

    const [name, setName] = useState({firstName : '', lastName : ''})

    const clickHandler = () => {
        setItem([...items, {
            key : items.length,
            value : Math.floor(Math.random() * 10 + 1)
        }])
    }

    const [items, setItem] = useState([])

    useEffect(() => {
        document.title = `Clicked : ${count} Times`
    }, [count])

    return (
        <>
            <p>You Clicked {count} Times </p>
            <button onClick={() => setCounter(count + 1)}>Increment 1</button>
            <button onClick={() => setCounter(count - 1)}>Decrement 1</button>
            <button onClick={updateFive}>Decrement 5</button>
            <br/>
            <div>
                <input type={"text"} value={name.firstName} onChange={event => setName({...name, firstName: event.target.value})}/>
                <input type={"text"} value={name.lastName} onChange={event => setName({...name, lastName: event.target.value})}/>
                <p>Name : {name.firstName} , Family : {name.lastName}</p>
            </div>
            <br/>
            <button onClick={clickHandler}>Insert a value</button>
            <ul>
                {
                    items.map(item => {
                        return(
                        <li key={item.key}>{item.value}</li>
                        )
                    })
                }
            </ul>

        </>
    )
}

export default HookCounter