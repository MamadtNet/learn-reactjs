import React, {useState, useEffect} from "react";

function HookTimer() {
    const [tick, setTick] = useState(0)
    useEffect(() => {
        const interval = setInterval(() => setTick(prevState => prevState + 1), 1000)
        return () => {
            clearInterval(interval)
        }
    }, [tick])
    return(
        <div>
            timer : {tick} second
        </div>
    )
}

export default HookTimer