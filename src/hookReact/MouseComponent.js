import React, {Component} from 'react'

class MouseComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            x: 0,
            y: 0
        }
    }

    logMouseMove = (event) => {
        this.setState({
            x: event.clientX,
            y: event.clientY
        })
    }

    componentDidMount() {
        window.addEventListener('mousemove', this.logMouseMove)
    }
    componentWillUnmount() {
        window.removeEventListener('mousemove', this.logMouseMove)
    }

    render() {
        return (
            <>
                <p>Mouse Position : x : {this.state.x} -  y : {this.state.y}</p>
            </>
        )


    }
}

export default MouseComponent