import React, {useState, useEffect} from "react";

function MouseHook(){
    const [x, setX] = useState(0)
    const [Y, setY] = useState(0)

    const logMouseMove = (event) => {
        setX(event.clientX)
        setY(event.clientY)
    }
    useEffect(() => {
        window.addEventListener('mousemove', logMouseMove)
        return() => {
            window.removeEventListener('mousemove', logMouseMove) //clean up in useEffect
        }
    }, [])
    return(
        <>
            <p>Mouse Position : x : {x} -  y : {Y}</p>
        </>
    )
}
export default MouseHook