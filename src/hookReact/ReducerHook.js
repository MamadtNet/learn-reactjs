import React, {useReducer} from "react";

const initialValue = {
    counterOne: 0,
    counterTwo: 10,
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'increment' :
            return {...state, counterOne: state.counterOne + action.value}
        case 'decrement' :
            return {...state, counterOne: state.counterOne - action.value}
        case 'incrementTwo' :
            return {...state, counterTwo: state.counterTwo + action.value}
        case 'decrementTwo' :
            return {...state, counterTwo: state.counterTwo - action.value}
        default :
            return state
    }
}

function ReducerHook() {
    // const [currentState, dispatch] = useReducer(reducer, initialValue)// arg1 : reducer func, arg2 : initial Value
    // dispatch : how to do action
    // reducer(currentState, action)// action : what do you want

    const [count, dispatch] = useReducer(reducer, initialValue)
    //and also you can use 2, 3, 4, ... useReducer

    return (
        <div>
            <h3>useReducer</h3>
            Count : {count.counterOne}
            <br/>
            <button onClick={() => dispatch({type: 'increment', value: 1})}>Increment</button>
            <button onClick={() => dispatch({type: 'decrement', value: 1})}>Decrement</button>
            <button onClick={() => dispatch({type: 'increment', value: 5})}>Increment 5</button>
            <button onClick={() => dispatch({type: 'decrement', value: 5})}>Decrement 5</button>
            <br/>
            Count : {count.counterTwo}
            <br/>
            <button onClick={() => dispatch({type: 'incrementTwo', value: 1})}>Increment</button>
            <button onClick={() => dispatch({type: 'decrementTwo', value: 1})}>Decrement</button>
        </div>
    )
}

export default ReducerHook