import React from "react";

function ButtonName(props) {
    console.log('ButtonName rendered')
    return (
        <>
            <button onClick={props.nameHandler}>
                {props.children}
            </button>
        </>
    )
}

export default React.memo(ButtonName)