import React from "react";

function Name(props){
    console.log('button rendered')
    return(
        <p>
            {props.name}
        </p>
    )
}

export default React.memo(Name)