import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'
import Header from "./Header";
import UpdateExample from "./UpdateExample";
import Unmount from "./Unmount";
import LearnFormCssSass from "./LearnFormCssSass";
import LiftingStateUp from "./LiftingStateUp"
import Composition from "./Composition"
import Routing from "./Routing"
import Context from "./Context";
import RefDemo from "./RefDemo"
import DesCounterClick from "./DesCounterClick";
import Fragment from "./Fragment"
import RenderProps from "./RenderProps"
import HttpReq from "./HttpReq";
import HookApp from "./hookReact/App"

//Expressions in jsx
let str = "Hello"
const myfirstelement = <h1>{str} React!</h1>
//without jsx
const myelement = React.createElement('h1', {}, 'I do not use JSX!');

//Insert a Large Block of HTML
const blockHTMl = ( //you can't use two tag !
    <ul>
        <li>Apples</li>
        <li>Bananas</li>
        <li>Cherries</li>
    </ul>
)
const fragment = ( // whit Fragment You can use two Block
    <React.Fragment>
        <h1>Hello, ReactWorld!</h1>
        <input type={"text"}/> {/*close element with '/>' */}
        <h2>Use Fragment with JSX</h2>
    </React.Fragment>
)
//components return HTML elements -> same purpose as js functions
// we have two components -> 1) class, 2) function
// class -> start with upper case letter and extends React.Component -> see App.js
//----------------------------------

//function

function Hello() {
    return (<h1>Hello, World</h1>)
}


ReactDOM.render(fragment, document.getElementById('index'));
ReactDOM.render(<App name="label_props" color="red"/>, document.getElementById('App'));
ReactDOM.render(<Header favcol={"yellow"}/>, document.getElementById('Header'));// you can use <Hello/>
ReactDOM.render(<UpdateExample/>, document.getElementById('UpdateExample'));
ReactDOM.render(<Unmount/>, document.getElementById('index'));
ReactDOM.render(<LearnFormCssSass/>, document.getElementById('LearnFormCssSass'))
ReactDOM.render(<LiftingStateUp/>, document.getElementById('LiftingStateUp'))
ReactDOM.render(<Composition/>, document.getElementById('Composition'))
ReactDOM.render(<Routing/>, document.getElementById('Routing'))
ReactDOM.render(<Context/>, document.getElementById('Context'))
ReactDOM.render(<RefDemo/>, document.getElementById('RefDemo'))
ReactDOM.render(<DesCounterClick myName="Mohammad"/>, document.getElementById('DesCounterClick'))
ReactDOM.render(<Fragment/>, document.getElementById('Fragment'))
ReactDOM.render(<RenderProps/>, document.getElementById('RenderProps'))
ReactDOM.render(<HttpReq/>, document.getElementById('Http-request'))
ReactDOM.render(<HookApp/>, document.getElementById('Http-request'))